Compound Designer
-------------------

Project Type: School Project.
Version: 1.0.

Details:
The main objective of this project is to enable users to fiddle around with chemistry compounds.
Technologies used for development: LAMP stack for back-end, HTML5, Javascript, jQuery, AJAX for front-end.
Technologies used for deployment: Heroku and Git.
