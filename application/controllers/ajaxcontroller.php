<?php

class AjaxController extends Controller
{
	function index()
	{
		$this->setRenderMode('none');
	}

	function about()
	{
		$this->setRenderMode('html');
	}

	function edge()
	{
		$graph = Session::get("graph_data");

		if ($graph == null) {
			$this->setErrorCode(404);
			$this->setRenderMode('none');
		} else {
			if (isset($_POST['node']) && isset($_POST['edge'])) {
				$node = $_POST['node'];
				$edge = $_POST['edge'];

				if (isset($graph[$node])) {
					if (isset($graph[$node]["list"][$edge])) {
						if (($edgeprops = $this->Ajax->edgeProperties($node, $edge, $graph)) != 0) {
							$this->set('result', $edgeprops);
							$this->set('request', 'edge');
							$this->setErrorCode(200);
							$this->setRenderMode('json');
						} else {
							$this->setErrorCode(400);
							$this->setRenderMode('none');
						}
					} else {
						$this->setErrorCode(404);
						$this->setRenderMode('none');
					}
				} else {
					$this->setErrorCode(404);
					$this->setRenderMode('none');
				}
			} else {
				$this->setErrorCode(404);
				$this->setRenderMode('none');
			}
		}
	}

	function node()
	{
		$graph = Session::get("graph_data");

		if ($graph == null) {
			$this->setErrorCode(404);
			$this->setRenderMode('none');
		} else {
			if (isset($_POST['node'])) {
				$node = $_POST['node'];

				if (isset($graph[$node])) {
					if (($nodeprops = $this->Ajax->nodeProperties($node, $graph)) != 0) {
						$this->set('result', $nodeprops);
						$this->set('request', 'node');
						$this->setErrorCode(200);
						$this->setRenderMode('json');
					} else {
						$this->setErrorCode(400);
						$this->setRenderMode('none');
					}
				} else {
					$this->setErrorCode(404);
					$this->setRenderMode('none');
				}
			}
			else {
				$this->setErrorCode(404);
				$this->setRenderMode('none');
			}
		}
	}

	function graph()
	{
		$graph = Session::get("graph_data");

		if ($graph == null) {
			$this->setErrorCode(404);
			$this->setRenderMode('none');
		} else {
			if (($graph_data = $this->Ajax->compoundData($graph)) != 0) {

				$this->set('result', $graph_data);
				$this->set('request', 'graph');
				$this->setErrorCode(200);
				$this->setRenderMode('json');

			} else {
				$this->setErrorCode(400);
				$this->setRenderMode('none');
			}
			
		}
	}

	function update()
	{
		$this->setRenderMode('none');
		$this->setErrorCode(200); // OK

		/* Store graph on server */
		$raw_post_data = file_get_contents("php://input");

		$graph = json_decode($raw_post_data, true);

		Session::set("graph_data", $graph);
	}

	function request()
	{
		$graph = Session::get("graph_data");
		if ($graph == null) {
			$this->setRenderMode('none');
			$this->setErrorCode(404);
		}
		else {
			$this->setRenderMode('json');
			$this->setErrorCode(200);
			$this->set('result', $graph);
		}
	}
}