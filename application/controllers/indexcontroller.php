<?php

class IndexController extends Controller {
   
    function index() {
        Session::init();
        
        $this->setRenderMode('html');
        $this->set('title', 'Compound Designer');
        $this->set('version', APPLICATION_VERSION);
    }
}
