<?php

class DataController extends Controller {

	protected $_elements = array("C", "N", "O", "Cl", "Na", "I");
	protected $_valence  = array(  4,   3,   2,    1,    1,   1);
	protected $_name     = array("Carbon", "Nitrogen", "Oxygen", "Chlorine", "Sodium", "Iodine");

	function index() {
		$this->setRenderMode('none');
	}

	function fetch() {
		$this->setRenderMode('json');

		$this->set('element_count', count($this->_elements));

		$json_result = array();
		for ( $i = 0; $i < count($this->_elements); $i++) {
			$json_result[] = array("symbol" => $this->_elements[$i], "valence" => $this->_valence[$i], "name" => $this->_name[$i]);
		}

		$this->set('elements', $json_result);
	}

	function fetch_element($index)
	{
		if (!ctype_digit($index))
		{
			$this->setRenderMode('none');
			return;
		}

		if ($index < 0 || $index > 7)
		{
			$this->setRenderMode('none');
			return;
		}

		$this->setRenderMode('json');
		$this->set('element', $this->_elements[$index]);
	}
}