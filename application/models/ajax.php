<?php

class Ajax extends Model
{
	/* Element Data */

	protected $_elements = array("C", "N", "O", "Cl", "Na", "I");
	protected $_valence  = array(  4,   3,   2,    1,    1,   1);
	protected $_name     = array("Carbon", "Nitrogen", "Oxygen", "Chlorine", "Sodium", "Iodine");

	protected $_mass 	 = array(
		"C" => 12,
		"H" => 1,
		"N" => 14,
		"O" => 16,
		"Cl" => 35.5,
		"Na" => 23,
		"I" => 127,
		"S" => 32,
	);

	protected $_el_val 	 = array(
		"C" => 4,
		"H" => 1,
		"N" => 3,
		"O" => 2,
		"Cl" => 1,
		"Na" => 1,
		"I" => 1,
		"S" => 2
	);

	public function nodeProperties($node_index, $graph)
	{
		$result_array = array();
		$linked = array();

		$node = $graph[$node_index];

		$result_array["element"] = $this->_name[$node["element_index"]];

		$occupied_links = 0;

		foreach ($node["list"] as $link) {
			if (!isset($linked[$graph[$link["idx"]]["element"]["symbol"]]))
				$linked[$graph[$link["idx"]]["element"]["symbol"]] = 0;
			$linked[$graph[$link["idx"]]["element"]["symbol"]]++;
			$occupied_links += $link["type"];
		}

		$linked["H"] = $node["element"]["valence"] - $occupied_links;

		$result_array["links"] = $linked;
		unset($linked); /* garbage collection */

		if ($node["element"]["symbol"] == "C") {
			$hybridization = 1;
			$carbon_type = 0;

			foreach ($node["list"] as $link) {
				$hybridization += $link["type"] - 1;
				if ($graph[$link["idx"]]["element_index"] == 0)
					$carbon_type++;
			}

			$result_array["hybridization"] = 3 - $hybridization + 1;
			$result_array["carbon_type"] = $carbon_type;
			unset($carbon_type);
			unset($hybridization); /* garbage collection */
		}

		/* Compound-related stuff */

		$compound = $this->compoundData($graph);

		if ($compound == 0)
			return 0;

		/* Compute percentages for this node */
		/* Mass percentage  ( this node's mass / compound's mass ) */
		$result_array["mass_percent"] = (float)($this->_mass[$node["element"]["symbol"]]);
		$result_array["mass_percent"] /= $compound["compound_mass"];
		$result_array["mass_percent"] *= 100;

		/* Garbage collection */
		unset($compound);

		/* Element specific trivia: */

		$result_array["trivia"] = $this->wikiElementData($node["element_index"]);
		$result_array["wiki_link"] = "http://en.wikipedia.org/wiki/" . $node["element"]["name"];

		return $result_array;
	}

	public function edgeProperties($node_idx, $edge_idx, $graph) {
		$result_array = array();

		$node1_idx = $node_idx;
		$node2_idx = $graph[$node_idx]["list"][$edge_idx]["idx"];

		$result_array["elements"] = array(
			array("node_idx" => intval($node1_idx),
				"element_idx" => $graph[$node1_idx]["element_index"],
				"element" => $graph[$node1_idx]["element"]),
			array("node_idx" => $node2_idx,
				"element_idx" => $graph[$node2_idx]["element_index"],
				"element" => $graph[$node2_idx]["element"]),
		);

		$result_array["type"] = $graph[$node1_idx]["list"][$edge_idx]["type"];

		$result_array["bonds"] = array(
			"sigma" => 1,
			"pi" 	=> 0
		);

		if ($result_array["type"] == 2)
			$result_array["bonds"]["pi"] = 1;

		if ($result_array["type"] == 3)
			$result_array["bonds"]["pi"] = 2;

		return $result_array;
	}

	public function compoundData($graph)
	{
		/*
		 * - iupac name
		 * - usual name 
		 * - chemical formula 
		 * - empirical formula
		 * - procentual composition
		 * - degree of unsaturation
		 * - sigma bonds
		 * - pi bonds
		 * - carbon types
		 */

		$result_array = array();

		$result_array["iupac_name"] = "N/A";
		$result_array["usual_name"] = "N/A";

		$element_count = array(
			"C" => 0,
			"H" => 0,
			"N" => 0,
			"O" => 0,
			"Cl" => 0,
			"Na" => 0,
			"I" => 0,
			"S" => 0,
		);

		$nodes_count = 0;

		$element_count["H"] = 0;

		/* Sigma and Pi bonds, plus carbon types */

		$sigma = 0;
		$pi = 0;
		$carbon_types = array(0, 0, 0, 0, 0);

		foreach ($graph as $idx => $node) {
			if (isset($node)) {
				$element_count[$node["element"]["symbol"]]++;

				$nodes_count++;

				/* Get neighbors */
				/* Count carbons */

				$neighbor_carbons = 0;
				$occupied_links = 0;

				if ($node["element"]["symbol"] == "C") {
					foreach ($node["list"] as $link) {
						if ($graph[$link["idx"]]["element"]["symbol"] == "C")
							$neighbor_carbons++;
					}

					$carbon_types[$neighbor_carbons]++;
				}
				
				/* Sigma and Pi, get neighbors only with higher indexes, so 
				 * each edge is only counted once */

				foreach ($node["list"] as $link) {
					if ($idx < $link["idx"]) {
						if ($link["type"] >= 1)
							$sigma++;
						if ($link["type"] >= 2)
							$pi++;
						if ($link["type"] >= 3)
							$pi++;
					}
					$occupied_links += $link["type"];
				}

				$element_count["H"] += $node["element"]["valence"] - $occupied_links;
			}
		}

		$result_array["carbon_types"] = $carbon_types;
		$result_array["bonds"] = array(
			"sigma" => $sigma,
			"pi" => $pi
		);

		/* some garbage collection */
		unset($sigma);
		unset($pi);
		unset($carbon_types);

		/* Chemical and empirical formulas, procentual composition, and degree of unsaturation */

		if ($nodes_count == 0)
			return 0;

		$gcd = 0;

		$du = 2;
		$result_array["c_formula"] = array();
		$result_array["e_formula"] = array();
		$result_array["proc_comp"] = array();

		$total_mass = 0.0;

		foreach (array("C", "H", "O", "N", "Cl", "I", "Na", "S") as $el) {
			if (isset($element_count[$el]) && $element_count[$el] != 0) {
				if ($gcd == 0)
					$gcd = $element_count[$el];
				else {
					$gcd = Utility::gcd($gcd, $element_count[$el]);
				}

				$result_array["c_formula"][] = array($el, $element_count[$el]);
				$result_array["e_formula"][] = array($el, $element_count[$el]);
				$result_array["proc_comp"][] = array($el, (float)($element_count[$el] * $this->_mass[$el]));
				$total_mass += (float)($element_count[$el] * $this->_mass[$el]);
				$du += $element_count[$el] * ($this->_el_val[$el] - 2);
			}
		}

		for ($i = 0; $i < count($result_array["e_formula"]); $i++) {
			$result_array["e_formula"][$i][1] /= $gcd;
			$result_array["proc_comp"][$i][1] /= $total_mass;
			$result_array["proc_comp"][$i][1] *= 100;
		}

		if (!$this->validGraph($graph)) { 
			return 0;
		}

		$result_array["du"] = $du / 2;
		$result_array["compound_mass"] = $total_mass;

		/* some garbage collection */ 
		unset($du);
		unset($gcd);
		unset($total_mass);

		return $result_array;
	}

	public function wikiElementData($el_idx)
	{
		$element_name = $this->_name[$el_idx];

		if ($element_name == "Carbon") {
			return array(
				"introduction" => "Carbon is the chemical element with symbol C and atomic number 6. As a member of group 14 on the periodic table, it is nonmetallic and tetravalent—making four electrons available to form covalent chemical bonds. There are three naturally occurring isotopes, with <sup>12</sup>C and <sup>13</sup>C being stable, while <sup>14</sup>C is radioactive, decaying with a half-life of about 5,730 years.",

				"properties" => array(
					"general" => array(
						array("Name, symbol, number", "Carbon, C, 6"),
						array("Element category", "polyatomic nonmetal, sometimes considered a metalloid"),
						array("Group, period, block", "14, 2, p"),
						array("Atomic Weight", "12.011"),
					),

					"history" => array(
						array("Discovery", "Egyptians and Sumerians (3750 BC)"),
						array("Recognized by", "Antoine Lavoisier (1789)"),
					),

					"physical" => array(
						array("Phase", "solid"),
						array("Density", "1.8–2.1 g·cm <sup>-3</sup>"),
						array("Sublimation Point", "3915 K, 3642 °C, 6588 °F"),
						array("Triple Point", "4600 K, 10800 kPa"),
						array("Heat of fusion", "117 (graphite) kJ·mol<sup>−1</sup>"),
						array("Molar heat capacity", "6.155 (diamond)<br>8.517 (graphite) J·mol<sup>−1</sup>·K<sup>−1</sup>"),
					),

					"atomic" => array(
						array("Oxidation states", "4, 3, 2, 1, 0, −1, −2, −3, −4"),
						array("Electronegativity", "2.55 (Pauling scale)"),
						array("Covalent radius", "77(sp3), 73(sp2), 69(sp) pm"),
						array("Van der Waals radius", "170 pm"),
					),
				),

				"ending" => "",
			);
		}

		if ($element_name == "Nitrogen") {
			return array(
				"introduction" => "Nitrogen, symbol N, is the chemical element of atomic number 7. At room temperature, it is a gas of diatomic molecules and is colorless and odorless. Nitrogen is a common element in the universe, estimated at about seventh in total abundance in our galaxy and the Solar System. On Earth, the element is primarily found as the free element; it forms about 80% of the Earth's atmosphere. The element nitrogen was discovered as a separable component of air, by Scottish physician Daniel Rutherford, in 1772.",

				"properties" => array(
					"general" => array(
						array("Name, symbol, number", "Nitrogen, N, 7"),
						array("Element category", "diatomic nonmetal"),
						array("Group, period, block", "15, 2, p"),
						array("Atomic Weight", "14.007"),
					),

					"history" => array(
						array("Discovery", "Daniel Rutherford (1772)"),
						array("Recognized by", "Jean-Antoine Chaptal (1790)"),
					),

					"physical" => array(
						array("Phase", "gas"),
						array("Density", "1.251 g/L"),
						array("Melting Point", "63.15 K, −210.00 °C, −346.00 °F"),
						array("Boiling Point", "77.355 K, −195.795 °C, −320.431 °F"),
						array("Triple Point", "63.151 K, 12.52 kPa"),
						array("Critical Point", "126.192 K, 3.3958 MPa"),
						array("Heat of fusion", "(N<sub>2</sub>) 0.72 kJ·mol<sup>−1</sup>"),
						array("Heat of vaporization", "(N<sub>2</sub>) 5.56 kJ·mol<sup>−1</sup>"),
						array("Molar heat capacity", "(N<sub>2</sub>) 29.124 J·mol<sup>−1</sup>·K<sup>−1</sup>"),
					),

					"atomic" => array(
						array("Oxidation states", "5, 4, 3, 2, 1, −1, −2, −3"),
						array("Electronegativity", "3.04 (Pauling scale)"),
						array("Covalent radius", "71±1 pm"),
						array("Van der Waals radius", "155 pm"),
					),
				),

				"ending" => "",
			);
		}

		if ($element_name == "Oxygen") {
			return array(
				"introduction" => "Oxygen is a chemical element with symbol O and atomic number 8. It is a member of the chalcogen group on the periodic table and is a highly reactive nonmetallic element and oxidizing agent that readily forms compounds (notably oxides) with most elements. By mass, oxygen is the third-most abundant element in the universe, after hydrogen and helium. At STP, two atoms of the element bind to form dioxygen, a diatomic gas that is colorless, odorless, and tasteless; with the formula O<sub>2</sub>.",

				"properties" => array(
					"general" => array(
						array("Name, symbol, number", "Oxygen, O, 8"),
						array("Element category", "diatomic nonmetal, chalcogen"),
						array("Group, period, block", "16, 2, p"),
						array("Atomic Weight", "15.999"),
					),

					"history" => array(
						array("Discovery", "Carl Wilhelm Scheele (1772)"),
						array("Recognized by", "Antoine Lavoisier (1777)"),
					),

					"physical" => array(
						array("Phase", "gas"),
						array("Density", "1.429 g/L"),
						array("Melting Point", "54.36 K, -218.79 °C, -361.82 °F"),
						array("Boiling Point", "90.188 K, -182.962 °C, -297.332 °F"),
						array("Triple Point", "54.361 K, 0.1463 kPa"),
						array("Critical Point", "154.581 K, 5.043 MPa"),
						array("Heat of fusion", "(O<sub>2</sub>) 0.444 kJ·mol<sup>−1</sup>"),
						array("Heat of vaporization", "(O<sub>2</sub>) 6.82 kJ·mol<sup>−1</sup>"),
						array("Molar heat capacity", "(O<sub>2</sub>) 29.378 J·mol<sup>−1</sup>·K<sup>−1</sup>"),
					),

					"atomic" => array(
						array("Oxidation states", "2, 1, −1, −2"),
						array("Electronegativity", "3.44 (Pauling scale)"),
						array("Covalent radius", "66±2 pm"),
						array("Van der Waals radius", "152 pm"),
					),
				),

				"ending" => "",
			);
		}

		if ($element_name == "Chlorine") {
			return array(
				"introduction" => "Chlorine is a chemical element with symbol Cl and atomic number 17. Chlorine is in the halogen group (17) and is the second lightest halogen after fluorine. The element is a yellow-green gas under standard conditions, where it forms diatomic molecules. It has the highest electron affinity and the third highest electronegativity of all the reactive elements; for this reason, chlorine is a strong oxidizing agent. Free chlorine is rare on Earth, and is usually a result of direct or indirect oxidation by oxygen.",

				"properties" => array(
					"general" => array(
						array("Name, symbol, number", "Chlorine, Cl, 17"),
						array("Element category", "diatomic nonmetal"),
						array("Group, period, block", "17, 3, p"),
						array("Atomic Weight", "35.45"),
					),

					"history" => array(
						array("Discovery", "Carl Wilhelm Scheele (1774)"),
						array("Recognized by", "Carl Wilhelm Scheele (1774)"),
					),

					"physical" => array(
						array("Phase", "gas"),
						array("Density", "3.2 g/L"),
						array("Melting Point", "171.6 K, -101.5 °C, -150.7 °F"),
						array("Boiling Point", "239.11 K, -34.04 °C, -29.27 °F"),
						array("Critical Point", "416.9 K, 7.991 MPa"),
						array("Heat of fusion", "(Cl<sub>2</sub>) 6.406 kJ·mol<sup>−1</sup>"),
						array("Heat of vaporization", "(Cl<sub>2</sub>) 20.41 kJ·mol<sup>−1</sup>"),
						array("Molar heat capacity", "(Cl<sub>2</sub>) 33.949 J·mol<sup>−1</sup>·K<sup>−1</sup>"),
					),

					"atomic" => array(
						array("Oxidation states", "7, 6, 5, 4, 3, 2, 1, -1"),
						array("Electronegativity", "3.16 (Pauling scale)"),
						array("Covalent radius", "102±4 pm"),
						array("Van der Waals radius", "175 pm"),
					),
				),

				"ending" => "",
			);
		}

		if ($element_name == "Sodium") {
			return array(
				"introduction" => "Sodium is a chemical element with the symbol Na (from Latin: natrium) and atomic number 11. It is a soft, silver-white, highly reactive metal and is a member of the alkali metals; its only stable isotope is <sup>23</sup>Na. The free metal does not occur in nature, but instead must be prepared from its compounds; it was first isolated by Humphry Davy in 1807 by the electrolysis of sodium hydroxide. Sodium is the sixth most abundant element in the Earth's crust, and exists in numerous minerals such as feldspars, sodalite and rock salt. Many salts of sodium are highly water-soluble, and their sodium has been leached by the action of water so that chloride and sodium (NaCl) are the most common dissolved elements by weight in the Earth's bodies of oceanic water.",

				"properties" => array(
					"general" => array(
						array("Name, symbol, number", "Sodium, Na, 11"),
						array("Element category", "alkali metal"),
						array("Group, period, block", "1, 3, s"),
						array("Atomic Weight", "22.98976928"),
					),

					"history" => array(
						array("Discovery", "Humphry Davy (1807)"),
						array("Recognized by", "Humphry Davy (1807)"),
					),

					"physical" => array(
						array("Phase", "solid"),
						array("Density", "0.968 g·cm<sup>−3</sup>"),
						array("Melting Point", "370.944 K, 97.794 °C, 208.029 °F"),
						array("Boiling Point", "1156.090 K, 882.940 °C, 1621.292 °F"),
						array("Critical Point", "(extrapolated) 2573 K, 35 MPa"),
						array("Heat of fusion", "2.60 kJ·mol<sup>−1</sup>"),
						array("Heat of vaporization", "97.42 kJ·mol<sup>−1</sup>"),
						array("Molar heat capacity", "28.230 J·mol<sup>−1</sup>·K<sup>−1</sup>"),
					),

					"atomic" => array(
						array("Oxidation states", "1, −1"),
						array("Electronegativity", "0.93 (Pauling scale)"),
						array("Covalent radius", "166±9 pm"),
						array("Van der Waals radius", "227 pm"),
					),
				),

				"ending" => "",
			);
		}

		if ($element_name == "Iodine") {
			return array(
				"introduction" => "Iodine is a chemical element with symbol I and atomic number 53. The name is from Greek ἰοειδής ioeidēs, meaning violet or purple, due to the color of elemental iodine vapor.<br> Iodine and its compounds are primarily used in nutrition, and industrially in the production of acetic acid and certain polymers. Iodine's relatively high atomic number, low toxicity, and ease of attachment to organic compounds have made it a part of many X-ray contrast materials in modern medicine. Iodine has only one stable isotope. A number of iodine radioisotopes are also used in medical applications.",

				"properties" => array(
					"general" => array(
						array("Name, symbol, number", "iodine, I, 53"),
						array("Element category", "diatomic nonmetal"),
						array("Group, period, block", "17, 5, p"),
						array("Atomic Weight", "126.90447"),
					),

					"history" => array(
						array("Discovery", "Bernard Courtois (1811)"),
						array("Recognized by", "Bernard Courtois (1811)"),
					),

					"physical" => array(
						array("Phase", "solid"),
						array("Density", "4.933 g·cm<sup>−3</sup>"),
						array("Melting Point", "386.85 K, 113.7 °C, 236.66 °F"),
						array("Boiling Point", "457.4 K, 184.3 °C, 363.7 °F"),
						array("Triple Point", "386.65 K, 12.1 kPa"),
						array("Critical Point", "819 K, 11.7 MPa"),
						array("Heat of fusion", "(I<sub>2</sub>) 15.52 kJ·mol<sup>−1</sup>"),
						array("Heat of vaporization", "(I<sub>2</sub>) 41.57 kJ·mol<sup>−1</sup>"),
						array("Molar heat capacity", "(I<sub>2</sub>) 54.44 J·mol−<sup>1·</sup>K<sup>−1</sup>"),
					),

					"atomic" => array(
						array("Oxidation states", "7, 5, 3, 1, -1"),
						array("Electronegativity", "2.66 (Pauling scale)"),
						array("Covalent radius", "139±3 pm"),
						array("Van der Waals radius", "198 pm"),
					),
				),

				"ending" => "",
			);
		}

		return array(
			"introduction" => "Unknown element.",
			"properties" => array(),
			"ending" => "",
		);
	}

	public function validGraph($graph) {
		/* BFS */
		$start_node = -1;

		foreach ($graph as $idx => $node) 
		{
			if (isset($node) && !is_null($node)) {
				$start_node = $idx;
				break;
			}
		}

		$nodes_count = 0;

		foreach ($graph as $idx => $node) 
		{
			if (isset($node) && !is_null($node)) {
				$nodes_count++;
			}
		}

		$mark_array = array();

		if ($start_node == -1)
			return false;

		$queue_array = array();
		$queue_array[] = $start_node;
		$mark_array[$start_node] = 1;

		for ($nodeIndex = 0; $nodeIndex < count($queue_array); $nodeIndex++) {
			$node = $graph[$queue_array[$nodeIndex]];
			if (isset($node) && !is_null($node)) {
				foreach ($node["list"] as $neighbor) {
					if (!isset($mark_array[$neighbor["idx"]])) {
						$mark_array[$neighbor["idx"]] = 1;
						$queue_array[] = $neighbor["idx"];
					}
				}
			}
		}

		if (count($mark_array) == $nodes_count)
			return true;
		else
			return false;
	}
}