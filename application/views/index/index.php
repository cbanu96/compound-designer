<div id="content-wrapper">
	<div id="content">
		<div id="app-container" style="display:none">
			<div id="canvas-container">
				<h1 class="unselectable">
					<a id="compound-props-link" href="#">Generate</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a id="compound-clear-link" href="#">Clear</a>
				</h1>
				<hr>
			</div>
		</div>
		<!--[if IE]>
		<div id="ie-test" class="center" style="display: none">
			This web application doesn't support Internet Explorer.
			Please choose from
			<a href="https://www.google.com/intl/en/chrome/browser/">Chrome</a>,
			<a href="http://getfirefox.com">Firefox</a> or <a href="http://www.apple.com/safari/">Safari</a>.
		</div>
		<![endif]-->
		<![if !IE]>
		<div id="js-test" class="center">
			This web application makes extensive use of JavaScript.
			Please make sure that it is enabled.
		</div>
		<![endif]>
		<div id="progress" style="visibility:hidden" class="unselectable default-pointer">
			<div id="progress-img">
			</div>
			<div id="progress-status">
				Loading...
			</div>
		</div>
	</div>
</div>