    <footer style="display: none">
        <div id="copy-container" class="unselectable default-pointer">
            <div id="copy-text">
                <a id="about" href="ajax/about" rel="author" class="ajax">About</a>
            </div>
        </div>
    </footer>
    <div id="ctxmenu-container">
        <div id="ctxmenu">
            <ul>
            </ul>
        </div>
    </div>
    <div id="popup"><div id="popup-content"></div></div>
    <div id="popup-overlay"></div>
    </body>
    <![if !IE]>
    <script type="text/javascript">
    function init_app() {
        /* Our application code is loaded. */

        /* Set application settings. */

        var app_container_width = Math.max(980, Math.round(60*$(document).width()/100));
        application.sidetab_manip.set_width(40*app_container_width/100 - 32);
        application.sidetab_manip.set_height(Math.round(80*($(document).height())/100 - 72));
        

        application.canvas_manip.set_width(Math.round(60*app_container_width/100));
        application.canvas_manip.set_height(Math.round(80*($(document).height())/100 - 72 - 52));

        /* Run application */
        application.run();
    }

    function load() {
        /* Hide JSTest, Show Progress, Begin Loading */
        /* 1. Hide JS Test */
        var jstest_div = document.getElementById("js-test");
        jstest_div.setAttribute("style", "display: none");

        /* 2. Show Progress */
        var progress_div = document.getElementById("progress");
        progress_div.setAttribute("style", "visibility: visible");
    
        /* 3. configure loader */
        page_loader.set_end_load_callback(function() {
            var progress_status_div = document.getElementById("progress-status");
            progress_status_div.innerHTML = "Loaded!";
            var progress_bar_div = document.getElementById("progress-img");
            progress_bar_div.setAttribute("style", "display: none");

            var old_onload = window.onload;

            window.onload = function() {
                if (old_onload)
                    old_onload();

                init_app();
            }
        });

        page_loader.set_update_msg_callback(function(msg) {
            var progress_status_div = document.getElementById("progress-status");
            progress_status_div.innerHTML = msg;
        });
    
        page_loader.set_error_callback(function() {
            var progress_status_div = document.getElementById("progress-status");
            progress_status_div.innerHTML = "Error while loading. Please try again later!";
            var progress_bar_div = document.getElementById("progress-img");
            progress_bar_div.setAttribute("style", "display: none");
        });

        page_loader.add_dep('static/js/application.js', 'js', "Loading application...");

        /* Slide header down and footer up */
        $("header").animate({
                top: "-=72"
            }, 500, function() {
            $("header").show().animate({
            top: "+=72",
            }, 500, function(){}); 
            $("footer").show(300);
        });

        popup.init();

        /* Set up $("#about") handler */
        $("#about").click(function() {
            var href = $(this).attr("href");

            $.ajax({
                url: href,
                success: function(data) {
                    popup.show(data);
                }
            });

            return false;
        });

        /* 4. start loader after page has been completely loaded */
        page_loader.start_load();
    }

    if (document.readyState === "complete")
        load();
    else {
        window.addEventListener("DOMContentLoaded", function() {
            load();
        }, false);
    }
    </script>
    <![endif]>
</html>