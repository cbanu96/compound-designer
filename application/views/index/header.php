<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?=$title?></title>
        
        <meta name="description" content="Chemistry Compound Designer">
        <meta name="viewport" content="width=device-width">

        <link rel="stylesheet" type="text/css" href="static/css/fonts.css">
        <link rel="stylesheet" type="text/css" href="static/css/normalize.min.css">
        <link rel="stylesheet" type="text/css" href="static/css/main.css">
        <link rel="stylesheet" type="text/css" href="static/css/load.css">

        <!--<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Tulpen+One">
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Marvel:400italic">
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Vollkorn">
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans">-->

        <script src="static/js/vendor/jquery-1.10.1.min.js"></script>
        
        <script src="static/js/loader.js"></script>
        <script src="static/js/popup.js"></script>
        <script>
            var ROOT_URL = "<?=ROOT_URL?>";
        </script>
    </head>
    <body>
        <header style="display: none">
            <div id="title-container" class="unselectable default-pointer">
                <div id="app-title">
                    Compound Designer
                </div>
            </div>
        </header>