<h1 class="about-title">About Compound Designer</h1>
<hr>
<p class="about-text">
	This is a school project developed by Cristian Banu.
	The main objective of this project is to enable users to fiddle around with chemistry compounds. <br>
	Technologies used for development: LAMP stack for back-end, HTML5, Javascript, jQuery, AJAX for front-end. <br>
	Technologies used for deployment: Heroku and Git. <br>
</p>
<p class="about-text">
	Application version <?=APPLICATION_VERSION;?>. 
</p>
<p class="about-text">
	Application code available at <a href="https://bitbucket.org/cbanu96/compound-designer/">Bitbucket</a>
</p>
<p class="about-text">
	Licensed under GPL v2, available <a href="http://www.gnu.org/licenses/gpl-2.0.txt">here</a>.
</p>