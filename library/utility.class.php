<?php

class Utility {
    public static function redirect($url) {
        header("Location: $url");
        exit;
    }

    public static function gcd($a, $b) {

    	while ($b != 0) {
    		$r = $a % $b;
    		$a = $b;
    		$b = $r;    		
    	}

    	return $a;
    }
}