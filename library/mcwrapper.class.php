<?php

class MCWrapper {
    private static $_mchandler = false;
    
    public static function set($key, $value) {
        if (!MCWrapper::$_mchandler) {
            MCWrapper::connect();
        }
        return MCWrapper::$_mchandler->set($key, $value);
    }
    
    public static function get($key) {
        if (!MCWrapper::$_mchandler) {
            MCWrapper::connect();
        }
        return MCWrapper::$_mchandler->get($key);
    }
    
    public static function connect() {
        MCWrapper::$_mchandler = new Memcache;
        MCWrapper::$_mchandler->connect('127.0.0.1', 11211);
    }
}