<?php

class Template {
    protected $variables = array();
    protected $_controller;
    protected $_action;
    protected $_mode;
    
    public function __construct($controller, $action, $mode = 'html') {
        $this->_controller = $controller;
        $this->_action = $action;
        $this->_mode = strtolower($mode);
        
        $permitted_modes = array('json', 'html', 'none', 'plaintext');
        if (!in_array($mode, $permitted_modes))
            trigger_error ("Invalid render mode ( Controller = $controller, Action = $action)", E_USER_ERROR);
    }
    
    public function setMode($mode) {
        $this->_mode = $mode;
    }
    
    public function set($key, $value) {
        $this->variables[$key] = $value;
    }
    
    private function renderHtml() {
        extract($this->variables);
        
        if (file_exists(ROOT_FOLDER . DS . 'application' . DS . 'views' . DS . $this->_controller . DS . 'header.php'))
            include ROOT_FOLDER . DS . 'application' . DS . 'views' . DS . $this->_controller . DS . 'header.php';
        else
            include ROOT_FOLDER . DS . 'application' . DS . 'views' . DS . 'header.php';
        
        include ROOT_FOLDER . DS . 'application' . DS . 'views' . DS . $this->_controller . DS . $this->_action . '.php';
        
        if (file_exists(ROOT_FOLDER . DS . 'application' . DS . 'views' . DS . $this->_controller . DS . 'footer.php'))
            include ROOT_FOLDER . DS . 'application' . DS . 'views' . DS . $this->_controller . DS . 'footer.php';
        else
            include ROOT_FOLDER . DS . 'application' . DS . 'views' . DS . 'footer.php';
    }
    
    public function render() {
        switch ($this->_mode) {
            default: 
                /** Intentionally omitting break */
            case 'html':
                $this->renderHtml();
                break;
            case 'plaintext':
                if(isset($this->variables['content']))
                    echo $this->variables['content'];
                break;
            case 'json': /** JSON output of protected $variables */
                echo json_encode($this->variables);
                break;
            case 'none': /** Blank */
                break;
        }
    }
}