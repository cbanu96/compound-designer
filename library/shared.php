<?php

function __autoload($class) {
    if (file_exists(ROOT_FOLDER . DS . 'library' . DS . strtolower($class) . '.class.php')) {
        require_once(ROOT_FOLDER . DS . 'library' . DS . strtolower($class) . '.class.php');
    } else if (file_exists(ROOT_FOLDER . DS . 'application' . DS . 'controllers' . DS . strtolower($class) . '.php')) {
        require_once(ROOT_FOLDER . DS . 'application' . DS . 'controllers' . DS . strtolower($class) . '.php');
    } else if (file_exists(ROOT_FOLDER . DS . 'application' . DS . 'models' . DS . strtolower($class) . '.php')) {
        require_once(ROOT_FOLDER . DS . 'application' . DS . 'models' . DS . strtolower($class) . '.php');
    } else {        
        trigger_error("Error: Couldn't load $class", E_USER_ERROR);
    }
}

function setErrorReporting() {
    error_reporting(E_ALL);
    if (DEBUG_ENVIRON == true) {
        ini_set('display_errors', 'On');
    } else {
        ini_set('display_errors', 'Off');
        ini_set('log_errors', 'On');
        ini_set('error_log', ROOT_FOLDER . DS . 'tmp' . DS . 'log' . DS . 'error.log' );
    }
}

function stripSlashesMQ($value) {
    if (is_array($value))
        $value = array_map ('stripSlashesMQ', $value);
    else
        $value = stripslashes ($value);
    
    return $value;
}

function fixMagicQuotes() {
    if (get_magic_quotes_gpc()) {
        $_GET    = stripSlashesMQ($_GET);
        $_POST   = stripSlashesMQ($_POST);
        $_COOKIE = stripSlashesMQ($_COOKIE);
    }
}

function unregisterGlobals() {
    if (ini_get('register_globals')) {
        $array = array('_SESSION', '_POST', '_GET', '_COOKIE', '_REQUEST', '_SERVER', '_ENV', '_FILES');
        foreach ($array as $value) {
            foreach ($GLOBALS[$value] as $key => $var) {
                if ($var === $GLOBALS[$key]) {
                    unset($GLOBALS[$key]);
                }
            }
        }
    }
}

function mainCall() {
    global $url;
    
    $url = ltrim(rtrim($url, "/"), "/");
    $parts = array();
    $parts = explode("/", $url);

    $controller = isset($parts[0]) && $parts[0] != '' ? $parts[0] : 'index';
    array_shift($parts);
    
    $action = isset($parts[0]) && $parts[0] != '' ? $parts[0] : 'index';
    array_shift($parts);
    
    $query = (count($parts) > 0) ? $parts : array();
    
    $controllerName = $controller;
    $controller = ucwords($controller);
    $model = rtrim($controller, "s");
    $controller .= 'Controller';
    $dispatch = new $controller($model, $controllerName, $action);
    
    if (method_exists($controller, $action)) {
        call_user_func_array(array($dispatch, $action), $query);
    } else {
        trigger_error("$controller->$action() not found!", E_USER_ERROR);
    }
}

setErrorReporting();
fixMagicQuotes();
unregisterGlobals();
mainCall();