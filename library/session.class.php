<?php

class Session {
    public function __construct()
    {}
    
    public static function init() {
        @session_start();
    }
    
    public static function destroy() {
        if (!isset($_SESSION)) {
            Session::init();
        }
        session_destroy();
        unset($_SESSION);
    }
    
    public static function set($key, $value) {
        $_SESSION[$key] = $value;
    }
    
    public static function get($key) {
        if (isset($_SESSION[$key]))
            return $_SESSION[$key];
        return null;
    }
}