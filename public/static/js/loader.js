var page_loader = {
	/* General page-loading functions */
	
	loading: false, /* Is Currently Loading? */
	dep_count: 0, /* Number of dependencies */
	dependencies: [], /* List of sources to include */
	file_type: [], /* JS or CSS */
	display_msgs: [], /* List of messages to display when including files */

	error_occurred: false,
	
	add_dep: function(dep_src, dep_ext, dep_msg) {
		if (!this.loading)
		{
			this.dependencies.push(dep_src);
			this.display_msgs.push(dep_msg);
			this.file_type.push(dep_ext);
			this.dep_count++;
		}
	},

	rem_dep: function(dep_src) {
		if (!this.loading)
		{
			var idx = this.dependencies.indexOf(dep_src);
			if ( idx > -1 ) /* dep_src found */
			{
				this.dependencies.splice(idx);
				this.dep_count--;
			}
		}
	},

	/* Callbacks */
	end_load_callback: null,

	update_msg_callback: null,

	error_callback: null,

	/* Callbacks fns */

	set_end_load_callback: function(callback) {
		this.end_load_callback = callback;
	},

	set_update_msg_callback: function(callback) {
		this.update_msg_callback = callback;
	},

	set_error_callback: function(callback) {
		this.error_callback = callback;
	},

	error_proc: function() { 
		this.error_occurred = true;

		if (this.loading == false) {
			this.set_message("Error!");
		}
	},

	/* Loading functions */

	set_message: function(message) {
		this.update_msg_callback(message);
	},

	load_js: function(src) {
		var dom_head = document.getElementsByTagName('head')[0];
		/* Insert <script src="" type="text/javascript"></script> */
		var script_element = document.createElement("script");
		script_element.setAttribute("src", src);
		script_element.setAttribute("type", "text/javascript");
		script_element.setAttribute("async", "false");
		script_element.setAttribute("defer", "defer");

		/* Append it to <head>'s end */
		dom_head.insertBefore(script_element, dom_head.firstChild);
	},

	load_css: function(src)	{
		var dom_head = document.getElementsByTagName('head')[0];
		/* Insert <link rel="stylesheet" type="text/css" href="src"> */
		var link_element = document.createElement("link");
		link_element.setAttribute("rel", "stylesheet");
		link_element.setAttribute("type", "text/css");
		link_element.setAttribute("href", src);

		/* Append it to <head>'s end */
		dom_head.appendChild(link_element);
	},

	load_index: function(idx) {
		this.set_message(this.display_msgs[idx]);
		if (this.file_type[idx] == 'js')
			this.load_js(this.dependencies[idx]);
		if (this.file_type[idx] == 'css')
			this.load_css(this.dependencies[idx]);
	},

	start_load: function() {
		this.load_started = true;
		var i = 0;
		for ( i = 0; i < this.dep_count; i++ )
		{
			this.load_index(i);
			if (this.error_occurred)
			{
				this.error_callback();
				break;
			}
		}

		if (!this.error_occurred) {
			this.loading = false;
			this.end_load(); /* Finished loading */
		}
	},

	end_load: function() {
		this.end_load_callback();
	}
};