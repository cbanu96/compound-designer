var popup = {

	popup_div: null,
	overlay_div: null,
	content_div: null,

	width: null,
	height: null,

	init: function() {
		this.width = 360;
		this.height = 320;

		this.popup_div = $("#popup");
		this.overlay_div = $("#popup-overlay");
		this.content_div = $("#popup-content");

		this.overlay_div.click(this.hide);
	},

	center: function() {
		this.popup_div.css("top", Math.max(0, (($(window).height() - this.height) / 2) + $(window).scrollTop()) + "px");
		this.popup_div.css("left", Math.max(0, (($(window).width() - this.width) / 2) + $(window).scrollLeft()) + "px");

		this.popup_div.css("width", this.width);
		this.popup_div.css("height", this.height);
	},

	show: function(html) {
		this.center();
		this.content_div.html(html);

		this.popup_div.show();
		this.overlay_div.show();
	},

	hide: function() {
		$("#popup").hide();
		$("#popup-overlay").hide();
	}
}