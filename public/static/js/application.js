/* application.js
 * 
 * Main javascript file.
 * Implements interactive UI and backend communication.
 */

var constants = {
	EDGE_EPSILON: 7,
	FETCH_URL: "data/fetch",
	EDGE_PROP_URL: "ajax/edge",
	NODE_PROP_URL: "ajax/node",
	COMPOUND_PROP_URL: "ajax/graph",
	UPDATE_GRAPH_URL: "ajax/update",
	REQUEST_GRAPH_URL: "ajax/request",
	ERROR_MESSAGE_EVAL: "Invalid server data.",
	ERROR_MESSAGE_FETCH: "Invalid fetch URL.",
	ERROR_CANVAS: "Couldn't load canvas. Invalid width or height specified.",
	ERROR_SIDEBAR: "Couldn't load sidebar. Invalid width or height specified.",
	CANVAS_FONT: "32px Open Sans",
};

var application = {
	chemistry_data: {
		parent: function() {
			return application;
		},

		data: null,

		fetch: function(from) {
			$.ajax({
				url: from,
				success: function(data) {
					try {
						application.chemistry_data.data = eval("(" + data + ")");
					} catch (e) {
						if (e instanceof SyntaxError) {
							console.log(constants.ERROR_MESSAGE_EVAL);
						}
					}
				},
				error: function() {
					console.log(constants.ERROR_MESSAGE_FETCH);
				},
				async: false
			});
		},

		init: function() {
			this.fetch(constants.FETCH_URL);
		}
	},

	backend_io: {
		parent: function() {
			return application;
		},

		send_graph: function(callback) {
			if (this.parent().data_graph.was_updated === true) {
				this.parent().data_graph.reset_updated();

				$.ajax({
					url: constants.UPDATE_GRAPH_URL,
					type: 'post',
					data: JSON.stringify(this.parent().data_graph.nodes),
					success: callback,
					error: function() {
						/* status bar + error logging */
						application.statusbar_manip.show("Unable to connect to server.");
					},
				});
			} else {
				callback();
			}
		},

		show_data: function(data) {
			application.sidetab_manip.preload(data.result, data.request);
		},

		request: function(what, parameters) {
			/*
			 * what = -> edge_properties
			 *      = -> node_properties
			 *      = -> graph_compute
			 * 
			 * parameters = JS object
			 * default parameters:
			 * {
			 * 		{"node": node_idx, "edge": list_idx},
			 *		-- no parameters for graph, since it must be already stored in a session on the server.
			 * }
			 *
			 */

			var fetch_url = '';
			if (what === "edge_properties") {
				fetch_url = constants.EDGE_PROP_URL;
			} else
			if (what === "node_properties") {
				fetch_url = constants.NODE_PROP_URL;
			} else
			if (what === "graph_compute") {
				fetch_url = constants.COMPOUND_PROP_URL;
			} else { /* Invalid option, exiting... */
				return;
			}

			/* Skeleton */
			this.send_graph(function() {
				$.ajax({
					url: fetch_url,
					type: 'post',
					data: parameters,

					success: application.backend_io.show_data,

					error: function(xhr, textStatus, errorThrown) {
						application.sidetab_manip.clear_content();

						/* Server-side error message */
						if (xhr.status == 400) {
							application.statusbar_manip.show("Invalid compound! Are all your atoms are connected?");
						}
						else
						if (xhr.status == 404) {
							application.statusbar_manip.show("Invalid request.");
						} else {
							application.statusbar_manip.show("Unknown server error.")
						}
					},

					dataType: 'json'
				});
			});
		},

		request_graph: function() {
			$.ajax({
				url: constants.REQUEST_GRAPH_URL,
				type: 'get',
				data: {},

				success: function(data) {
					application.data_graph.load_graph(data.result);
					application.canvas_manip.redraw_canvas();	
				},
				dataType: 'json'
			});
		},

		init: function() {
			this.request_graph();
			//this.parent().data_graph.set_updated(true);
			//this.send_graph();
		},
	},

	context_menu: {
		parent: function() {
			return application;
		},

		ctx_menu: null,
		ctx_menu_cont: null,

		ctx_list: {
			list: [],

			add: function(title, handler) {
				this.list.push({
					content: title,
					func: handler
				})
			},

			del: function(idx) {
				if (idx !== -1)
					this.list.splice(idx, 1);
			},

			reset: function() {
				this.list = [];
			}
		},

		init: function() {
			this.ctx_menu_cont 	= $("#ctxmenu-container");
			this.ctx_menu		= $("#ctxmenu");
		},

		show: function(x, y) {
			var ul = this.ctx_menu.children("ul");

			ul.empty(); /* Clear all event handlers etc */

			var ul_html = '';

			for ( var i = 0, len = this.ctx_list.list.length; i < len; i++)
			{
				var item = this.ctx_list.list[i];

				ul_html += '<li id="ctx-menu-item-'+i+'">'+item.content+'</li>';
			}

			/* Modify context menu */
			ul.html(ul_html);

			/* Add event handlers */
			for ( var i = 0, len = this.ctx_list.list.length; i < len; i++)
			{
				$("#ctx-menu-item-" + i).click(function() {
					var idx = $(this).attr("id").substr(14);

					application.context_menu.ctx_list.list[idx].func({
						xCoord: x-application.canvas_manip.canvas.offset().left,
						yCoord: y-application.canvas_manip.canvas.offset().top
					});

					application.context_menu.hide();

					return false;
				});
			}

			this.ctx_menu_cont.css("left", x + "px");
			this.ctx_menu_cont.css("top", y + "px");

			this.ctx_menu_cont.fadeIn(300);
		},

		hide: function() {
			this.ctx_menu_cont.hide();
			this.ctx_menu.children("ul").empty();
			this.ctx_list.reset();
		}
	},

	data_graph: {
		parent: function() {
			return application;
		},

		nodes: [],

		was_updated: false, /* Set to true whenever any new element/bond is inserted/deleted */

		init: function() {

		},

		load_graph: function(graph) {
			this.nodes = graph;
			this.was_updated = true;
		},

		set_updated: function(value) {
			this.was_updated = value;
		},

		add_node: function(xPos, yPos, el_idx) {
			for ( var i = 0, len = this.nodes.length; i < len; i++ )
			{
				if (this.nodes[i] === undefined) {
					this.nodes[i] = {
						x: xPos,
						y: yPos,
						element: this.parent().chemistry_data.data.elements[el_idx],
						element_index: el_idx,
						list: [], /* Adjacency list for node i */
						usedPos: [0, 0, 0, 0]
					};

					this.set_updated(true);

					return i;
				}
			}

			this.nodes.push({
				x: xPos,
				y: yPos,
				element: this.parent().chemistry_data.data.elements[el_idx],
				element_index: el_idx,
				list: [],
				usedPos: [0, 0, 0, 0]
			});

			this.set_updated(true);

			return this.nodes.length - 1;
		},

		delete_node: function(idx)
		{
			if (idx < this.nodes.length) {
				if (this.nodes[idx] !== undefined) {
					for ( var i = 0, len = this.nodes[idx].list.length; i < len; i++) {
						this.del_edge(this.nodes[idx].list[i].idx, idx);
					}

					this.set_updated(true);

					delete this.nodes[idx];
					return;
				}
			}
		},

		find_node_at: function(xPos, yPos) {
			for ( var i = 0, len = this.nodes.length; i < len; i++ )
			{
				if (this.nodes[i] !== undefined) {
					var dx = this.nodes[i].element.symbol.length * 24;
					var dy = 24;
					if((this.nodes[i].x <= xPos && xPos <= this.nodes[i].x + dx) &&
					   (this.nodes[i].y <= yPos && yPos <= this.nodes[i].y + dy))
					{
						return i;
					}
				}
			}
			return -1;
		},

		get_link_point: function(node, position, type) {
			var x = this.nodes[node].x;
			var y = this.nodes[node].y;
			var dx = this.nodes[node].element.symbol.length * 24;
			var dy = 28;

			var link_points = [];

			if (position === 0)
			{
				if (type === 1)
					link_points.push({"x": x, "y": y+dy/2});
				if (type === 2) {
					link_points.push({"x": x, "y": y+dy/2-3});
					link_points.push({"x": x, "y": y+dy/2+3});
				}
				if (type === 3) {
					link_points.push({"x": x, "y": y+dy/2-6});
					link_points.push({"x": x, "y": y+dy/2});
					link_points.push({"x": x, "y": y+dy/2+6});
				}
			}

			if (position === 1)
			{
				if (type === 1)
					link_points.push({"x": x+dx/2, "y": y+dy});
				if (type === 2) {
					link_points.push({"x": x+dx/2-3, "y": y+dy});
					link_points.push({"x": x+dx/2+3, "y": y+dy});
				}
				if (type === 3) {
					link_points.push({"x": x+dx/2-6, "y": y+dy});
					link_points.push({"x": x+dx/2, "y": y+dy});
					link_points.push({"x": x+dx/2+6, "y": y+dy});
				}
			}

			if (position === 2) {
				if (type === 1)
					link_points.push({"x": x+dx, "y": y+dy/2});
				if (type === 2) {
					link_points.push({"x": x+dx, "y": y+dy/2-3});
					link_points.push({"x": x+dx, "y": y+dy/2+3});
				}
				if (type === 3) {
					link_points.push({"x": x+dx, "y": y+dy/2-6});
					link_points.push({"x": x+dx, "y": y+dy/2});
					link_points.push({"x": x+dx, "y": y+dy/2+6});
				}
			}

			if (position === 3)
			{
				if (type === 1)
					link_points.push({"x": x+dx/2, "y": y});
				if (type === 2) {
					link_points.push({"x": x+dx/2-3, "y": y});
					link_points.push({"x": x+dx/2+3, "y": y});
				}
				if (type === 3) {
					link_points.push({"x": x+dx/2-6, "y": y});
					link_points.push({"x": x+dx/2, "y": y});
					link_points.push({"x": x+dx/2+6, "y": y});
				}
			}

			return link_points;
		},

		get_distance: function(xa, ya, xb, yb) {
			return Math.sqrt((xa-xb)*(xa-xb) + (ya-yb)*(ya-yb));
		},

		add_edge: function(node1, node2, type) {

			var posA, posB;

			var minDist = this.get_distance(0, 0, application.canvas_manip.width, application.canvas_manip.height);

			for ( var i = 0; i < 4; i++ ) {
				if (!this.nodes[node1].usedPos[i]) {
					for ( var j = 0; j < 4; j++ ) {
						if (!this.nodes[node2].usedPos[j]) {
							var link_pt1, link_pt2;

							link_pt1 = this.get_link_point(node1, i, 1)[0];
							link_pt2 = this.get_link_point(node2, j, 1)[0];

							var dist = this.get_distance(link_pt1.x, link_pt1.y, link_pt2.x, link_pt2.y);

							if (dist < minDist) {
								posA = i;
								posB = j;
								minDist = dist;
								}
						}
					}
				}
			}

			this.nodes[node1].list.push({
				positionA: posA,
				positionB: posB,
				idx: node2,
				type: type
			});

			this.nodes[node2].list.push({
				positionA: posB,
				positionB: posA,
				idx: node1,
				type: type
			});

			this.nodes[node1].usedPos[posA]++;
			this.nodes[node2].usedPos[posB]++;

			this.set_updated(true);
		},

		inner_del_edge: function(node1, node2) {
			if (this.nodes[node1] !== undefined) {
				for ( var i = 0, len = this.nodes[node1].list.length; i < len; i++ ) {
					if (this.nodes[node1].list[i].idx === node2) {
						this.nodes[node1].usedPos[this.nodes[node1].list[i].positionA]--;
						this.nodes[node1].list.splice(i, 1);

						this.set_updated(true);

						return;
					}
				}
			}
		},

		del_edge: function(node1, node2) {
			this.inner_del_edge(node1, node2);
			this.inner_del_edge(node2, node1);
		},

		can_link_nodes: function(node1, node2) {
			if (node1 === node2)
				return false;

			var deg1, valence1;
			var deg2, valence2;

			deg1 = deg2 = 0;

			valence1 = this.nodes[node1].element.valence;
			valence2 = this.nodes[node2].element.valence;

			/* Compute "degrees" */
			for ( var i = 0, len = this.nodes[node1].list.length; i < len; i++) {
				deg1 += this.nodes[node1].list[i].type;

				/* Check for already existing edge */
				if (this.nodes[node1].list[i].idx === node2)
					return false;
			}

			for ( var i = 0, len = this.nodes[node2].list.length; i < len; i++)
				deg2 += this.nodes[node2].list[i].type;

			if (deg1 >= valence1 || deg2 >= valence2)
				return false;

			return true;
		},

		get_node_empty_valences: function(node) {
			var value;

			value = this.nodes[node].element.valence;
			for ( var i = 0, len = this.nodes[node].list.length; i < len; i++ )  {
				value -= this.nodes[node].list[i].type;
			}

			return value;
		},

		get_distance_p2l: function(xP, yP, xA, yA, xB, yB)
		{
			if (xA < xB) {
				if (!(xA - constants.EDGE_EPSILON <= xP && xP <= xB + constants.EDGE_EPSILON))
					return false;
			} else {
				if (!(xB - constants.EDGE_EPSILON <= xP && xP <= xA + constants.EDGE_EPSILON))
					return false;
			}

			if (yA < yB) {
				if (!(yA - constants.EDGE_EPSILON <= yP && yP <= yB + constants.EDGE_EPSILON))
					return false;
			} else {
				if (!(yB - constants.EDGE_EPSILON <= yP && yP <= yA + constants.EDGE_EPSILON))
					return false;
			}

			var AB = this.get_distance(xA, yA, xB, yB);
			var S = xP * yA + xA * yB + xB * yP - xP * yB - xA * yP - xB * yA;

			if (S < 0)
				S = -S;

			return S / AB;
		},

		find_edge_at: function(xPos, yPos) {
			var min_dist = this.get_distance(0, 0, application.canvas_manip.width, application.canvas_manip.height);
			var min_node1 = -1;
			var min_node2 = -1;

			for ( var i = 0, len = this.nodes.length; i < len; i++ ) {
				var item = this.nodes[i];
				if (item !== undefined) {
					for ( var j = 0, len_adj = item.list.length; j < len_adj; j++ ) {
						if (i < item.list[j].idx) {
							var link_pt1 = this.get_link_point(i, item.list[j].positionA, 1)[0];
							var link_pt2 = this.get_link_point(item.list[j].idx, item.list[j].positionB, 1)[0];

							var dist = this.get_distance_p2l(xPos, yPos, link_pt1.x, link_pt1.y, link_pt2.x, link_pt2.y);
							if (dist !== false) {
								if (dist < min_dist) {
									min_dist = dist;
									min_node1 = i;
									min_node2 = item.list[j].idx;
								}
							}
						}
					}
				}
			}

			if (min_dist < constants.EDGE_EPSILON) {
				return {"node1": min_node1, "node2": min_node2};
			}

			return -1;
		},

		inner_set_edge_type: function(node1, node2, type) {
			if (this.nodes[node1] !== undefined) {
				for ( var i = 0, len = this.nodes[node1].list.length; i < len; i++ ) {
					if (this.nodes[node1].list[i].idx === node2) {
						this.nodes[node1].list[i].type = type;

						this.set_updated(true);

						return;
					}
				}
			}
		},

		set_edge_type: function(node1, node2, type) {
			this.inner_set_edge_type(node1, node2, type);
			this.inner_set_edge_type(node2, node1, type);
		},

		get_edge_type: function(node1, node2) {
			if (this.nodes[node1] !== undefined) {
				for ( var i = 0, len = this.nodes[node1].list.length; i < len; i++ ) {
					if(this.nodes[node1].list[i].idx === node2) {
						return this.nodes[node1].list[i].type;
					}
				}
			}

			return 0;
		},

		get_edge_types: function(node1, node2) {
			var edge_type = this.get_edge_type(node1, node2);
			if (edge_type !== 0)
			{
				/* Get valence-degree of node1 and node2 */
				var empty_val1 = this.get_node_empty_valences(node1);
				var empty_val2 = this.get_node_empty_valences(node2);

				var types = [];

				for ( var i = 1; i < edge_type; i++ ) {
					types.push(i);
				}

				var max_edge_type = Math.min(3, edge_type+Math.min(empty_val1, empty_val2));
				for ( var i = edge_type+1; i <= max_edge_type; i++ ) {
					types.push(i);
				}

				return types;
			}

			return [];
		},

		set_node_xy: function(node, x, y) {
			if (this.nodes[node] !== undefined) {
				this.nodes[node].x = x;
				this.nodes[node].y = y;
			}
		},

		reset_updated: function() {
			this.set_updated(false);
		},

		find_edge_list_idx: function(node1, node2) {
			var item = this.nodes[node1];
			if (item !== undefined) {
				for ( var j = 0, len_adj = item.list.length; j < len_adj; j++ ) {
					if (item.list[j].idx === node2)
						return j;
				}
			}
			return -1;
		},
	},

	statusbar_manip: {
		container: null,
		content: null,
		hideInterval: null,

		init: function() {
			var html = '<div id="statusbar-container">';
			html += '<div id="statusbar" style="display: none;">';
			html += '<div id="statusbar-content">';
			html += '</div>';
			html += '</div>';
			$("body").append(html);

			this.container = $("#statusbar");
			this.content = $("#statusbar-content");

			this.handler_init();
		},

		handler_init: function() {
			this.container.click(function() { 
				application.statusbar_manip.hide();
			});
		},

		hide: function() {
			if (this.hideInterval !== null)
				clearInterval(this.hideInterval);
			this.container.fadeOut(300);
		},

		show: function(message) {
			this.container.hide();
			this.content.html(message);
			this.container.fadeIn(300);
			if (this.hideInterval !== null)
				clearInterval(this.hideInterval);
			this.hideInterval = setInterval(function() {application.statusbar_manip.hide();}, 3000);
		}
	},

	sidetab_manip: {
		width: null,
		height: null,
		sidetab: null,
		sidetab_content: null,
		sidetab_progress: null,

		set_width: function(value) {
			this.width = value;
		},

		set_height: function(value) {
			this.height = value;
		},

		init: function() {
			if (this.width === null || this.height === null) {
				console.log(constants.ERROR_SIDEBAR);
				return;
			}

			var html = '<div id="sidetab-container"><div id="sidetab">';
			html += '<h1 class="unselectable default-pointer">Properties</h1><hr><div id="sidetab-content">';
			html += '</div><div id="sidetab-progress" style="left: ' + (application.canvas_manip.width + (this.width)/2) + 'px; ' + 'top: ' + (this.height)/2 + 'px; ' + 'display: none;"></div></div></div>';
			$("#app-container").append(html);

			this.sidetab = $("#sidetab");
			this.sidetab_content = $("#sidetab-content");
			this.sidetab_progress = $("#sidetab-progress");

			$("#sidetab-container").width(this.width);
			$("#sidetab-container").height(this.height);
			$("#sidetab-content").height(this.height - 64);
		},

		preload: function(data, request_type) {
			this.hide_content();
			this.show_progress();
			/* Marshalling data into sidetab content */
			this.sidetab_content.html(this.deserialize_data(data, request_type));
			this.show_content();
		},

		deserialize_data: function(data, request_type) {
			if (request_type === "edge") {
				var html = '<h1>';
				html += data.elements[0].element.symbol;
				if (data.type === 1) {
					html += '&mdash;';
				}
				if (data.type === 2) {
					html += '=';
				}
				if (data.type === 3) {
					html += '&equiv;';
				}
				html += data.elements[1].element.symbol;
				html += '</h1>';
				html += '<p>';
				if (data.type === 1) {
					html += 'Simple';
				}
				if (data.type === 2) {
					html += 'Double';
				}
				if (data.type === 3) {
					html += 'Triple';
				}
				html += ' bond (' + data.bonds.sigma + ' sigma';
				if(data.bonds.pi) html += ' and ' + data.bonds.pi + ' pi';
				html += ')';
				html += ' between a ' + data.elements[0].element.name + ' atom and another ' + data.elements[1].element.name + ' atom.';
				html += '</p>';


				return html;	
			}
			else
			if (request_type === "graph") {
				var html = '';

				html += '<h1>compound</h1><hr>';
				html += '<ul>';
				/* IUPAC / usual name */
				html += '<li><span class="propname">IUPAC Name</span>: ';
				html += '<span class="propval">' + data.iupac_name + '</span></li>';

				html += '<li><span class="propname">Usual Name</span>: ';
				html += '<span class="propval">' + data.usual_name + '</span></li>';

				html += '</ul>';

				html += '<h1>formulas</h1><hr>';
				html += '<ul>';

				/* C. Formula */
				html += '<li><span class="propname">Chemical Formula</span>: ';
				html += '<span class="propval">'
				if (data.hasOwnProperty("c_formula")) {
					if (data.c_formula) {
						for ( var i = 0, len = data.c_formula.length; i < len; i++ ) {
							html += data.c_formula[i][0] + '<sub>' + data.c_formula[i][1] + '</sub>';
						}
					}
					else html += "N/A";
				}
				else html += "N/A";
				html += '</span></li>';
				/* E. Formula */
				html += '<li><span class="propname">Empirical Formula</span>: ';
				html += '<span class="propval">'
				if (data.hasOwnProperty("e_formula")) {
					if (data.e_formula) {
						for ( var i = 0, len = data.e_formula.length; i < len; i++ ) {
							html += data.e_formula[i][0] + '<sub>' + data.e_formula[i][1] + '</sub>';
						}
					}
					else html += "N/A";
				}
				else html += "N/A";
				html += '</span></li>';
				/* Degree of Unsaturation */
				html += '<li><span class="propname">Degree of Unsaturation</span>: ';
				html += '<span class="propval">';
				html += data.du;
				html += '</span></li>';

				/* Compound mass */
				html += '<li><span class="propname">Compound mass</span>: ';
				html += '<span class="propval">';
				html += data.compound_mass;
				html += '</span></li>';

				html += '</ul>';

				/* Carbon types in compound */
				html += '<h1> carbon types </h1>';
				html += '<hr>';

				html += '<ul>';

				html += '<li>';
				html += '<span class="propname">nular carbons in compound</span>: ';
				html += '<span class="propval">' + data.carbon_types[0] + '</span>';
				html += '</li>';

				html += '<li>';
				html += '<span class="propname">primary carbons in compound</span>: ';
				html += '<span class="propval">' + data.carbon_types[1] + '</span>';
				html += '</li>';

				html += '<li>';
				html += '<span class="propname">secondary carbons in compound</span>: ';
				html += '<span class="propval">' + data.carbon_types[2] + '</span>';
				html += '</li>';

				html += '<li>';
				html += '<span class="propname">tertiary carbons in compound</span>: ';
				html += '<span class="propval">' + data.carbon_types[3] + '</span>';
				html += '</li>';

				html += '<li>';
				html += '<span class="propname">quaternary carbons in compound</span>: ';
				html += '<span class="propval">' + data.carbon_types[4] + '</span>';
				html += '</li>';

				html += '</ul>';

				html += '<h1>bonds</h1><hr>';
				html += '<ul>';

				html += '<li><span class="propname">sigma bonds</span>: ';
				html += '<span class="propval">' + data.bonds.sigma + '</span></li>';

				html += '<li><span class="propname">pi bonds</span>: ';
				html += '<span class="propval">' + data.bonds.pi + '</span></li>';

				html += '</ul>';

				html += '<h1>procentual composition</h1>';
				html += '<hr>';

				html += '<ul>';
				for ( var i = 0, len = data.proc_comp.length; i < len; i++ ) {
					if (data.proc_comp[i] !== undefined)
						if (data.proc_comp[i][1] != 0) {
							html += '<li><span class="propname">' + data.proc_comp[i][0] + '</span>: ';
							html += '<span class="propval">' + data.proc_comp[i][1].toFixed(2) + '%</span></li>';
						}
				}
				html += '</ul>';

				return html;
			}
			else
			if (request_type === "node") {
				var html = '';
				html += '<h1>' + data.element + ' Properties </h1>';
				html += '<h1>computed</h1>';
				html += '<hr>';
				html += '<ul>';
				
				if (data.carbon_type !== undefined) {
					html += '<li><span class="propname">carbon type</span>: <span class="propval">';
					if (data.carbon_type === 0)
						html += 'nular carbon';
					if (data.carbon_type === 1)
						html += 'primary carbon';
					if (data.carbon_type === 2)
						html += 'secondary carbon';
					if (data.carbon_type === 3)
						html += 'tertiary carbon';
					if (data.carbon_type === 4)
						html += 'quaternary carbon';
					html += '</span></li>';
				}

				if (data.hybridization) {
					html += '<li><span class="propname">hybridization</span>: <span class="propval">';
					if (data.hybridization === 1)
						html += 'sp1 hybridization';
					if (data.hybridization === 2)
						html += 'sp2 hybridization';
					if (data.hybridization === 3)
						html += 'sp3 hybridization';
					html += '</span></li>';
				}

				if (data.mass_percent) {
					html += '<li>';
					html += '<span class="propname">one atom of ' + data.element + ' represents</span>:  <span class="propval">' + data.mass_percent.toFixed(2) + '% of the compound\'s total mass</span>';
					html += '</li>';
				}

				if (data.links) {
					html += '<li>';
					html += '<span class="propname">linked to</span>: ';
					var used_comma = 0;
					html += '<span class="propval">'
					for ( var el in data.links ) {
						if (data.links.hasOwnProperty(el)) {
							if (data.links[el] != 0) {
								if (used_comma === 0)
									html += el + ' (' + data.links[el] + ' bond' + (data.links[el] != 1 ? 's' : '') + ')', used_comma = 1;
								else
									html += ', ' + el + ' (' + data.links[el] + ' bonds)';
							}
						}
					}
					html += '</span></li>';
				}

				html += '</ul>';

				/* Trivia */

				html += '<br>';
				html += '<h1><a href="' + data.wiki_link + '" target="_blank">trivia</a></h1><hr>';
				html += '<p>' + data.trivia.introduction + '</p>';
				for ( var prop_type in data.trivia.properties ) {
					if (data.trivia.properties.hasOwnProperty(prop_type)) {
						html += '<h1>' + prop_type + '</h1><hr>';
						html += '<ul>';
						for ( var i = 0, len = data.trivia.properties[prop_type].length; i < len; i++ ) {
							html += '<li> <span class="propname">' + data.trivia.properties[prop_type][i][0] + '</span>';
							html += ': ';
							html += '<span class="propval">' + data.trivia.properties[prop_type][i][1] + '</span></li>';
						}
						html += '</ul>';
					}
				}

				return html;
			}
			else
			{
				return "";
			}
		},

		hide_content: function() {
			this.sidetab_content.hide();
		},

		show_content: function() {
			this.hide_progress();
			this.sidetab_content.fadeIn(300);
		},

		show_progress: function() {
			this.sidetab_progress.fadeIn(100);
		},

		hide_progress: function() {
			this.sidetab_progress.fadeOut(100);
		},

		clear_content: function() {
			this.sidetab_content.html("");
		}
	},

	canvas_manip: {
		parent: function() {
			return application;
		},

		width: null,
		height: null,
		ctx: null,
		canvas: null,
		canvas_content: null, /* last saved canvas content */
		blank_canvas: null,

		set_width: function(val) {
			this.width = val;
		},

		set_height: function(val) {
			this.height = val;
		},

		init: function() {
			if (this.width === null || this.height === null) {
				console.log(constants.ERROR_CANVAS);
				return;
			}

			var html = '<canvas id="app-canvas" width="' + this.width + '" height="' + this.height + '">';
			html += 'Your browser doesn\'t support canvas HTML5 element. </canvas>';

			$("#canvas-container").append(html);

			this.canvas = $("#app-canvas");
			
			var el = document.getElementById("app-canvas");
			if (el.getContext) {
				this.ctx = el.getContext("2d");
			} else {
				console.log(constants.ERROR_CANVAS);
			}

			/* add handler for compound properties generator */

			$("#compound-props-link").click(function() {
				application.backend_io.request("graph_compute", {});
				return false;
			});

			$("#compound-clear-link").click(function() {
				application.data_graph.load_graph([]);
				application.canvas_manip.redraw_canvas();
				return false;
			});

			/* save canvas content after init */

			this.save_canvas_content();
			this.blank_canvas = this.ctx.getImageData(0, 0, this.width, this.height);
		},

		reload: function() {
			this.redraw_canvas();
		},

		save_canvas_content: function() {
			this.canvas_content = this.ctx.getImageData(0, 0, this.width, this.height);
		},

		load_canvas_content: function() {
			this.ctx.putImageData(this.canvas_content, 0, 0);
		},

		redraw_canvas: function() {
			this.ctx.putImageData(this.blank_canvas, 0, 0); /* Load empty canvas */
			for ( var i = 0, len = this.parent().data_graph.nodes.length; i < len; i++) {
				var item = this.parent().data_graph.nodes[i];
				if (item !== undefined) {
					this.draw_element(item.x, item.y, item.element_index);
					for ( var j = 0, len_adj = item.list.length; j < len_adj; j++ ) {
						if (i < item.list[j].idx) {
							var link_pt1 = application.data_graph.get_link_point(i, item.list[j].positionA, item.list[j].type);
							var link_pt2 = application.data_graph.get_link_point(item.list[j].idx, item.list[j].positionB, item.list[j].type);

							if ((item.list[j].positionA == 1 && item.list[j].positionB == 0) ||
								(item.list[j].positionA == 0 && item.list[j].positionB == 1) ||
								(item.list[j].positionA == 2 && item.list[j].positionB == 3) ||
								(item.list[j].positionA == 3 && item.list[j].positionB == 2)) {

								for ( var k = 0, type = item.list[j].type; k < type; k++) {
									this.draw_line_gfx(link_pt1[k].x, link_pt1[k].y, link_pt2[type-k-1].x, link_pt2[type-k-1].y, "#000000");
								}
							} else {
								for ( var k = 0, type = item.list[j].type; k < type; k++) {
									this.draw_line_gfx(link_pt1[k].x, link_pt1[k].y, link_pt2[k].x, link_pt2[k].y, "#000000");
								}
							}
						}
					}
				}
			}
		},

		draw_element: function(x, y, element, color)
		{
			/* Set up font, font size, etc */
			this.ctx.font = constants.CANVAS_FONT;

			if (color)
				this.ctx.fillStyle = color;

			this.ctx.fillText(this.parent().chemistry_data.data.elements[element].symbol, x, y+24);

			/* reset style */
			this.ctx.fillStyle = '#000000';
		},

		draw_at: function(x, y, element) {
			this.parent().data_graph.add_node(x, y, element);

			this.redraw_canvas();
			this.save_canvas_content();
		},

		draw_line_gfx: function(x1, y1, x2, y2, color)
		{
			this.ctx.beginPath();
			this.ctx.moveTo(x1, y1);
			this.ctx.lineTo(x2, y2);
			this.ctx.strokeStyle = color;
			this.ctx.stroke();

			/* Reset style */
			this.ctx.strokeStyle = "#000000";
		},

		draw_link: function(node1, node2) {
			application.data_graph.add_edge(node1, node2, 1);

			this.redraw_canvas();
			this.save_canvas_content();
		}
	},

	cursor_manip: {
		parent: function() {
			return application;
		},

		ctx_menu_handlers: 
		[
			function(data) { application.canvas_manip.draw_at(data.xCoord, data.yCoord, 0);},
			function(data) { application.canvas_manip.draw_at(data.xCoord, data.yCoord, 1);},
			function(data) { application.canvas_manip.draw_at(data.xCoord, data.yCoord, 2);},
			function(data) { application.canvas_manip.draw_at(data.xCoord, data.yCoord, 3);},
			function(data) { application.canvas_manip.draw_at(data.xCoord, data.yCoord, 4);},
			function(data) { application.canvas_manip.draw_at(data.xCoord, data.yCoord, 5);},
			function(data) {
				var idx = application.data_graph.find_node_at(data.xCoord, data.yCoord);
				if (idx !== -1)
					application.data_graph.delete_node(idx);

				application.canvas_manip.redraw_canvas();
				application.canvas_manip.save_canvas_content();
			},

			function(data) { /* Node properties */
				var idx = application.data_graph.find_node_at(data.xCoord, data.yCoord);
				if (idx !== -1)
					application.backend_io.request("node_properties", {"node": idx});
			},

			function(data) { /* Delete edge */
				var edge_data = application.data_graph.find_edge_at(data.xCoord, data.yCoord);
				if (edge_data !== -1) {
					application.data_graph.del_edge(edge_data.node1, edge_data.node2);

					application.canvas_manip.redraw_canvas();
					application.canvas_manip.save_canvas_content();
				}
			},

			function(data) { /* Convert to type=1 edge */
				var edge_data = application.data_graph.find_edge_at(data.xCoord, data.yCoord);
				if (edge_data !== -1) {
					application.data_graph.set_edge_type(edge_data.node1, edge_data.node2, 1);
					application.canvas_manip.redraw_canvas();
					application.canvas_manip.save_canvas_content();
				}
			},

			function(data) { /* Convert to type=2 edge */
				var edge_data = application.data_graph.find_edge_at(data.xCoord, data.yCoord);
				if (edge_data !== -1) {
					application.data_graph.set_edge_type(edge_data.node1, edge_data.node2, 2);
					application.canvas_manip.redraw_canvas();
					application.canvas_manip.save_canvas_content();
				}
			},

			function(data) { /* Convert to type=3 edge */
				var edge_data = application.data_graph.find_edge_at(data.xCoord, data.yCoord);
				if (edge_data !== -1) {
					application.data_graph.set_edge_type(edge_data.node1, edge_data.node2, 3);
					application.canvas_manip.redraw_canvas();
					application.canvas_manip.save_canvas_content();
				}
			},

			function(data) { /* Edge properties */
				var edge_data = application.data_graph.find_edge_at(data.xCoord, data.yCoord);
				if (edge_data !== -1) {
					var node = edge_data.node1;
					var edge = application.data_graph.find_edge_list_idx(node, edge_data.node2);
					if (edge !== -1) {
						application.backend_io.request("edge_properties", {"node": node, "edge": edge});
					}
				}
			},
		],

		ctx_menu_bind: function() {
			this.parent().canvas_manip.canvas.bind("contextmenu", function(event) {
				event.stopImmediatePropagation();
				event.preventDefault();

				/* Clear old shit */
				application.context_menu.hide();

				/* Load appropriate menu */
				var canvasX = event.pageX - $(this).offset().left;
				var canvasY = event.pageY - $(this).offset().top;
				var node_idx = application.data_graph.find_node_at(canvasX, canvasY);
				if (node_idx === -1) {
					var edge_idx = application.data_graph.find_edge_at(canvasX, canvasY);
					if (edge_idx !== -1) {
						var edge_types = application.data_graph.get_edge_types(edge_idx.node1, edge_idx.node2);
						var option_names = [undefined, "Simple", "Double", "Triple"];
						application.context_menu.ctx_list.add("Delete", application.cursor_manip.ctx_menu_handlers[8]);
						application.context_menu.ctx_list.add("Properties", application.cursor_manip.ctx_menu_handlers[12]);
						for (var i = 0, len = edge_types.length; i < len; i++ ) {
							application.context_menu.ctx_list.add(option_names[edge_types[i]], application.cursor_manip.ctx_menu_handlers[8+edge_types[i]]);
						}

					} else {
						for ( i = 0; i < application.chemistry_data.data.elements.length; i++)
							application.context_menu.ctx_list.add(application.chemistry_data.data.elements[i].name, 
																  application.cursor_manip.ctx_menu_handlers[i]);
					}
				} else {
					application.context_menu.ctx_list.add("Delete", application.cursor_manip.ctx_menu_handlers[6]);
					application.context_menu.ctx_list.add("Properties", application.cursor_manip.ctx_menu_handlers[7]);
				}
				application.context_menu.show(event.pageX, event.pageY);

				return false;
			});

			$(document).bind("mousedown", function(event) {
				if (!$(event.target).is("#ctxmenu ul li"))
					application.context_menu.hide();
			});
		},

		mouse_dragged: false,
		mouse_moving: false,
		mouse_begin_x: 0,
		mouse_begin_y: 0,
		mouse_event_ignore: false,
		mouse_drag_data: null,

		/* Left click functions */

		mouse_down_left: function(event) {
			application.context_menu.hide();

			this.mouse_begin_x = event.x;
			this.mouse_begin_y = event.y;

			var idx = application.data_graph.find_node_at(event.x, event.y);
			if (idx === -1) {
				this.mouse_event_ignore = true;
				return false;
			}

			application.canvas_manip.redraw_canvas();
			application.canvas_manip.save_canvas_content();

		},

		mouse_drag_left: function(event) {
			if (!this.mouse_event_ignore) {

				/* Draw canvas */
				application.canvas_manip.load_canvas_content();
				/* Add red-coloured line from beginning x,y to current event.(x,y). */
				application.canvas_manip.draw_line_gfx(this.mouse_begin_x, this.mouse_begin_y,
													   event.x, event.y, "#FF0000");
			}
		},

		mouse_up_left: function(event) {
			if (!this.mouse_event_ignore) {

				/* Redraw canvas */
				application.canvas_manip.load_canvas_content();

				if (event.x === this.mouse_begin_x && event.y === this.mouse_begin_y && this.mouse_dragged === false) {
					/* click */
				}
				else
				if (this.mouse_dragged === true) {
					/* drag */
					var begin_node = application.data_graph.find_node_at(this.mouse_begin_x, this.mouse_begin_y);
					var end_node   = application.data_graph.find_node_at(event.x, event.y);

					if (end_node === -1)
						return false;

					if (application.data_graph.can_link_nodes(begin_node, end_node)) {

						/* Link nodes and refresh canvas */

						application.canvas_manip.draw_link(begin_node, end_node);
						application.canvas_manip.redraw_canvas();
						application.canvas_manip.save_canvas_content();
					} else {
						application.statusbar_manip.show("Those two atoms are already bonded.");
					}
				}
			}
		},

		mouse_invalid_left: function(event) {
			/* This function is called when the mouseup event is out of bounds.
			 * Do some error handling code and redraw canvas. */
			if (!this.mouse_event_ignore) {

				application.canvas_manip.redraw_canvas();
				application.canvas_manip.save_canvas_content();
			}
		},

		/* Middle click functions */

		mouse_down_middle: function(event) {
			application.context_menu.hide();

			this.mouse_begin_x = event.x;
			this.mouse_begin_y = event.y;

			var idx = application.data_graph.find_node_at(event.x, event.y);
			if (idx === -1) {
				this.mouse_drag_data = null;
				this.mouse_event_ignore = true;
				return false;
			}

			this.mouse_drag_data = idx;

			application.canvas_manip.redraw_canvas();
			application.canvas_manip.save_canvas_content();
		},

		mouse_drag_middle: function(event) {
			if (!this.mouse_event_ignore) {

				/* Draw canvas */
				application.canvas_manip.load_canvas_content();
				/* Add red-coloured node at cursor position. */
				application.canvas_manip.draw_element(event.x, event.y, application.data_graph.nodes[this.mouse_drag_data].element_index, "rgba(255, 0, 0, 0.5)");
			}
		},

		mouse_up_middle: function(event) {
			if (!this.mouse_event_ignore) {
				/* Update node to current position */
				application.data_graph.set_node_xy(this.mouse_drag_data, event.x, event.y);

				this.mouse_drag_data = null;

				application.canvas_manip.redraw_canvas();
				application.canvas_manip.save_canvas_content();
			}
		},

		mouse_invalid_middle: function(event) {
			if (!this.mouse_event_ignore) {
				application.canvas_manip.redraw_canvas();
				application.canvas_manip.save_canvas_content();
			}
		},

		canvas_mouse_bind: function() {
			$("#app-canvas").bind("mousedown.LinkCreate", function(event) {
				if (event.which === 1) { /* Left Click */
					application.cursor_manip.mouse_moving = true;
					application.cursor_manip.mouse_dragged = false;
					application.cursor_manip.mouse_event_ignore = false;
					/* Fire begin event handler */
					var result = application.cursor_manip.mouse_down_left({
						x: event.pageX - application.canvas_manip.canvas.offset().left,
						y: event.pageY - application.canvas_manip.canvas.offset().top
					});

					/* If we got a false result from mouse_down_left, stop */
					if (result === false)
						return false;

					/* Canvas mouse bind is only called on init, but 
					 * the following handler is called on every mousedown.
					 * It's worth optimizing DOM Lookup */

					application.canvas_manip.canvas.bind("mousemove.LinkCreate", function(event) {
						application.cursor_manip.mouse_dragged = true;
						/* Fire drag event handler */
						application.cursor_manip.mouse_drag_left({
							x: event.pageX - application.canvas_manip.canvas.offset().left,
							y: event.pageY - application.canvas_manip.canvas.offset().top
						});
					});
				}

				if (event.which === 2) {
					application.cursor_manip.mouse_moving = true;
					application.cursor_manip.mouse_dragged = false;
					application.cursor_manip.mouse_event_ignore = false;
					/* Fire begin event handler */
					var result = application.cursor_manip.mouse_down_middle({
						x: event.pageX - application.canvas_manip.canvas.offset().left,
						y: event.pageY - application.canvas_manip.canvas.offset().top
					});

					/* If we got a false result from mouse_down_middle, stop */
					if (result === false)
						return false;

					/* Canvas mouse bind is only called on init, but 
					 * the following handler is called on every mousedown.
					 * It's worth optimizing DOM Lookup */

					application.canvas_manip.canvas.bind("mousemove.LinkCreate", function(event) {
						application.cursor_manip.mouse_dragged = true;
						/* Fire drag event handler */
						application.cursor_manip.mouse_drag_middle({
							x: event.pageX - application.canvas_manip.canvas.offset().left,
							y: event.pageY - application.canvas_manip.canvas.offset().top
						});
					});
				}
			});

			$(document).bind("mouseup.LinkCreate", function(event) {
				$("#app-canvas").unbind("mousemove.LinkCreate");
				if (event.which === 1) {
					if($(event.target).is("#app-canvas")) {
						application.cursor_manip.mouse_up_left({
							x: event.pageX - application.canvas_manip.canvas.offset().left,
							y: event.pageY - application.canvas_manip.canvas.offset().top
						});
					} else {

						if (application.cursor_manip.mouse_moving === true)
							application.cursor_manip.mouse_invalid_left();
					}
				}
				if (event.which === 2) {
					if($(event.target).is("#app-canvas")) {
						application.cursor_manip.mouse_up_middle({
							x: event.pageX - application.canvas_manip.canvas.offset().left,
							y: event.pageY - application.canvas_manip.canvas.offset().top
						});
					} else {

						if (application.cursor_manip.mouse_moving === true)
							application.cursor_manip.mouse_invalid_middle();
					}
				}

				application.cursor_manip.mouse_moving = false;
			});
		},

		init: function() {
			this.ctx_menu_bind();
			this.canvas_mouse_bind();
		}
	},

	init_components: function() {
		/* Backend modules */
		this.chemistry_data.init();
		this.data_graph.init();

		/* UI modules */
		this.canvas_manip.init();
		this.statusbar_manip.init();
		this.sidetab_manip.init();
		this.context_menu.init();
		this.cursor_manip.init();

		this.backend_io.init();
	},

	run: function() {
		this.init_components();

		$("#progress").hide({
			queue: false,
			duration: 600,
			complete: function() {
				$("#app-container").fadeIn(690);
			}
		});
	}
}
